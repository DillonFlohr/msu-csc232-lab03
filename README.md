# Lab 3 - Arrays and Pointers

See the Lab 3 hand for the details on how to complete this lab.

## Generating Projects

As usual, this repo contains a number of `cmake` generator options. For example, navigate to the `build/unix` folder and run `cmake` to generate Unix make files that can be used to easily create executables from the source code.

```bash
$ cd build/unix
$ cmake -G "Unix Makefiles" ../..
... lots of output ...
$ ./Lab03
... lab executable output ...
$
```